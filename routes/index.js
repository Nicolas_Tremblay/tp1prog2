var express = require('express');
var router = express.Router();
let blog = require("../Custom modules/blog")
let contacts = require("../Custom modules/contact")
let formation = require("../Custom modules/formation")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('Libri/index', { title: 'Acceuil'});
});
/* GET formation. */
router.get('/Nos_formations', function(req, res, next) {
  res.render('Libri/Nos_formations', { title: 'Nos formations'});
});

router.get('/Nos_formations-All_formation_by_type/:type', function(req, res, next) {
  res.render('Libri/Nos_formations-All_formation_by_type', { title: req.params.type, formations: formation.getAllFormation(req.params.type)});
});

router.get('/Nos_formations-Single_formation_by_type/:id', function(req, res, next) {
  res.render('Libri/Nos_formations-Single_formation_by_type', { title: 'Une formation', formation: formation.getFormationsById(req.params.id)});
})

/* GET blog. */
router.get('/Blog', function(req, res, next) {
  res.render('Libri/Blog', { title: 'Blog', blog: blog.allBlog});
});

router.get('/blog/:id', function(req, res, next) {
  res.render('Libri/Blog_article', {title: 'Article', blog: blog.getBlogById(req.params.id)})
})
/* GET nous contacter. */    
router.get('/Nous_contacter', function(req, res, next) {
  res.render('Libri/Nous_contacter', { title: 'Nous contacter'});
});

/* POST contact. */
router.post('/nous_contacter/add', function(req, res, next){
  let body = req.body
  contacts.addContact(body)
  let formulaire = contacts.validerFormulaire(body)
  if(formulaire.valeur){
    res.render("Libri/Nous_contacter-info_valide", { title: 'Nous contacter -> Informations valide', info: body})
  } else{
    res.render("Libri/Nous_contacter-info_invalide", { title: 'Nous contacter -> Erreur', erreur: formulaire.tableau, info: body})
  }
})

module.exports = router;
