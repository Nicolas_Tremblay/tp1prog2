
class Contact{
    constructor(id, nom, prenom, courriel, tel, sujet, champ){
    this.id = id
    this.nom = nom
    this.prenom = prenom
    this.courriel = courriel
    this.tel = tel
    this.sujet = sujet
    this.champ = champ
    }
}

let dataContact = []

exports.addContact = function(data){
    let id = dataContact.length
    if(id == 0){
        id = 1
    }
    dataContact.push(new Contact(id, data.nom, data.prenom, data.courriel, data.tel, data.sujet, data.champ))
    if(messageErreur.length > 0){
            let i
            i = id - 1
        dataContact.splice(i, 1)
        }
    console.log(dataContact)
    return true
}

exports.allContact = dataContact

let messageErreur = []

exports.validerFormulaire = function (data){
    messageErreur = []

    var index = dataContact.length -1
    let nom = dataContact[index].nom
    let prenom = dataContact[index].prenom
    let courriel = dataContact[index].courriel
    let tel = dataContact[index].tel
    let champ = dataContact[index].champ
    // Verification des champs
    validerNom(nom)

    validerPrenom(prenom)

    validerCourriel(courriel)

    validerTel(tel)

    validerChamp(champ)
    // fin de la validation
    if(messageErreur.length > 0){
        console.log(messageErreur)
        return {
            valeur: false,
            tableau: messageErreur,
        }
    } else {
        return {
            valeur: true,
            tableau: messageErreur,
        }
    }
}

function validerNom(nom){
    const nomValide = /[^a-z0-9 ]/i
    if (nom === ''){
        messageErreur.push(' -> Le nom est vide')
    } else if(nomValide.test(nom)){
        messageErreur.push(' -> Le nom est invalide')
        }
}

function validerPrenom(prenom){
    const nomValide = /[^a-z0-9 ]/i
    if (prenom === ''){
        messageErreur.push(' -> Le prenom est vide')
    } else if(nomValide.test(prenom)){
        messageErreur.push(' -> Le prenom est invalide')
        }
}

function validerCourriel(courriel){
    const emailValide = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (courriel === ''){
        messageErreur.push(' -> Le courriel est vide')
    } else if(!emailValide.test(courriel)){
        messageErreur.push(' -> Le courriel est invalide')
        }
}

function validerTel(tel) {
    const telValide = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/
    if (tel === ''){
        messageErreur.push(' -> Le téléphone est vide')
    } else if(!telValide.test(tel)){
        messageErreur.push(' -> Le numéro de téléphone est invalide')
        }
}

function validerChamp(champ) {
    if (champ === ''){
        messageErreur.push(' -> La demande est vide')
        }
}
// exports.erreurDeValidation = messageErreur