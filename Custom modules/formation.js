class formation{
    constructor(id, type, titre, description, duree, cout){
        this.id = id
        this.type = type
        this.titre = titre
        this.description = description
        this.duree = duree
        this.cout = cout
    }
}

const data = [
    new formation( 0, "MongoDB", "Mongo1", "Ca fait beaucoup de noix ça", "90 heures", "200$"),
    new formation( 1, "MongoDB", "Mongo2", "Ca fait beaucoup de noix ça", "60 heures", "400$"),
    new formation( 2, "MongoDB", "Mongo3", "Ca fait beaucoup de noix ça", "45 heures", "600$"),
    new formation( 3, "MongoDB", "Mongo4", "Ca fait beaucoup de noix ça", "90 heures", "800$"),
    new formation( 4, "MongoDB", "Mongo5", "Ca fait beaucoup de noix ça", "60 heures", "1200$"),
    new formation( 5, "Nodejs", "Node1", "Ca fait beaucoup de noix ça", "90 heures", "200$"),
    new formation( 6, "Nodejs", "Node2", "Ca fait beaucoup de noix ça", "60 heures", "400$"),
    new formation( 7, "Nodejs", "Node3", "Ca fait beaucoup de noix ça", "45 heures", "600$"),
    new formation( 8, "Nodejs", "Node4", "Ca fait beaucoup de noix ça", "90 heures", "800$"),
    new formation( 9, "Nodejs", "Node5", "Ca fait beaucoup de noix ça", "60 heures", "1200$"),
]

exports.getFormationsById = function(id){
    return data[id]
}

exports.getFormationByType = function(type){
    const typeTableau = []
    for(let i = 0; i > data.length; i++) {
        if(data[i].type == type){
            typeTableau.push(type)
        }
    }
    return typeTableau
}
exports.getAllFormation = function(type_param){
    const tableau = []
    for(let i = 0; i < data.length; i++){
        if(data[i].type == type_param){
            tableau.push(data[i])
        }
    }
    return tableau
}