class Blog{
    constructor(id, titre, date, auteur, contenu){
    this.id = id
    this.titre = titre
    this.date = date
    this.auteur = auteur
    this.contenu = contenu
    }
}

const dataBlog = [
    new Blog( 0, "titre 1", "20-01-2022", "Paul Lachance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 1, "titre 2", "20-02-2022", "Paul PaD'chance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 2, "titre 3", "20-03-2022", "Paul Lachance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 3, "titre 4", "20-04-2022", "Paul PaD'chance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 4, "titre 5", "20-05-2022", "Paul Lachance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 5, "titre 6", "20-06-2022", "Paul PaD'chance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 6, "titre 7", "20-07-2022", "Paul Lachance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 7, "titre 8", "20-08-2022", "Paul PaD'chance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 8, "titre 9", "20-09-2022", "Paul Lachance", "Contenant le contenu nu dans un contexte de texte con."),
    new Blog( 9, "titre 10", "20-10-2022", "Paul PaD'chance", "Contenant le contenu nu dans un contexte de texte con."),
]

exports.getBlogById = function(id){
    return dataBlog[id]
}

exports.allBlog = dataBlog